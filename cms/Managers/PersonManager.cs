﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using cms.Models;
using cms.Stores;

namespace cms.Managers
{
    public class PersonManager
    {
        private readonly PersonStore store;

        public PersonManager(PersonStore store)
        {
            this.store = store;
        }

        public List<Person> FindAllASync()
        {
            return this.store.FindAllASync();
        }

        public Person FindById(string id)
        {
            return this.store.FindById(id);
        }

        public async Task<bool> UpdateAsync(Person person)
        {
           return await this.store.UpdateAsync(person);
        }
    }
}