﻿using System.Web.Http;

namespace cms
{
    public class WebApiConfig
    {
        public static void Register(HttpConfiguration config)
        {
            config.MapHttpAttributeRoutes();

            config.Routes.MapHttpRoute(
                name: "DefaultApi",
                routeTemplate: "api/person/{id}",
                defaults: new { controllers="Home", id = RouteParameter.Optional }
            );
        }
    }
}
