﻿using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;

using cms.Models;

namespace cms.Database
{
    public partial class Database
    {

        public List<T> Insert<T>(string id, Person person)
            where T : class
        {
            //INSERT INTO table_name (column1, column2, column3,..) VALUES ( value1, value2, value3,..)
            var type = typeof(T);
            var table = type.Name;
            var hashtable = new Hashtable();
            var properties = type.GetProperties();
            foreach (var info in properties)
            {
                hashtable[info.Name] = info;
            }

            return new List<T>();
        }
    }
}
