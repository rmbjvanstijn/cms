﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Net;
using System.Net.Http;
using System.Reflection;
using System.Threading.Tasks;
using System.Web.Http;
using System.Web.Http.Results;

using cms.Models;

namespace cms.Database
{
    public partial class Database
    {

        public async Task<bool> Update<T>(T model)
            where T : class
        {
            //INSERT INTO table_name (column1, column2, column3,..) VALUES ( value1, value2, value3,..)
            var type = typeof(T);
            var table = type.Name;
            var hashtable = new Hashtable();
            var properties = type.GetProperties();
            var query = new String[]{};
            var count = 0;
            foreach (var info in properties)
            {
                //hashtable[info.Name] = info.GetValue(model, null);
                query[count] = $"{info.Name}='{info.GetValue(model, null)}'";
                count++;
            }

            //
            //var query = $"UPDATE dbo.{table} SET FirstName='Jan', Age='27' WHERE Id = 1";

            //using (SqlConnection conn = new SqlConnection())
            //{
            //    conn.ConnectionString = "Server=ROBERT-LAPTOP\\SQLEXPRESS;Database=Cms;Trusted_Connection=true";
            //    var command = new SqlCommand(query, conn);

            //    conn.Open();
            //    command.ExecuteNonQuery();
            //}

            return true;
        }
    }
}
