﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.Reflection;

namespace cms.Database
{
    public partial class Database
    {

        public List<T> Get<T>(string param)
            where T : class
        {
            var dbList = new List<T>();
            var type = typeof(T);
            var table = type.Name;

            var hashtable = new Hashtable();
            var properties = type.GetProperties();
            foreach (var info in properties)
            {
                hashtable[info.Name.ToUpper()] = info;
            }

            var query = $"SELECT * FROM {table}";
            var queryWithParams = !string.IsNullOrWhiteSpace(param) ? $"{query} {param}" : query; 
            using (SqlConnection conn = new SqlConnection())
            {

                conn.ConnectionString = "Server=ROBERT-LAPTOP\\SQLEXPRESS;Database=Cms;Trusted_Connection=true";
                conn.Open();
                var command = new SqlCommand(queryWithParams, conn);
                using (SqlDataReader reader = command.ExecuteReader())
                {

                    while (reader.Read())
                    {
                        var newObj = Activator.CreateInstance<T>(); ;
                        for (var i = 0; i < reader.FieldCount; i++)
                        {
                            var info = (PropertyInfo)hashtable[reader.GetName(i).ToUpper()];

                            if ((info != null) && info.CanWrite)
                            {
                                info.SetValue(newObj, reader.GetValue(i), null);
                            }
                        }

                        dbList.Add(newObj);
                    }
                }
            }

            return dbList;
        }
    }
}
