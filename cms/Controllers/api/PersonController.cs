﻿using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

using cms.Models;
using cms.Database;
using cms.Managers;

using Microsoft.AspNetCore.JsonPatch;

namespace cms.Controllers.api
{
    //https://docs.microsoft.com/en-us/aspnet/mvc/overview/older-versions/hands-on-labs/aspnet-mvc-4-dependency-injection#Exercise1
    [RoutePrefix("api/person")]
    public class PersonController : ApiController
    {
        private readonly Database.Database database;
        private readonly PersonManager personManager;

        public PersonController(Database.Database database, PersonManager personManager)
        {
            this.database = database;
            this.personManager = personManager;
        }

        //[Authorize]
        [HttpGet]
        [Route("")]
        public IHttpActionResult GetAll()
        {
            var persons = this.personManager.FindAllASync();
            return Ok(persons);
        }

        [HttpGet]
        [Route("{id}")]
        public IHttpActionResult GetOne(string id)
        {
            var person = this.personManager.FindById(id);
            return Ok(person);
        }

        [HttpPatch]
        [Route("{id}")]
        public async Task<IHttpActionResult> UpateOne([FromBody]JsonPatchDocument<Person> personPatch, string id)
        {
            if (personPatch == null)
            {
                return BadRequest();
            }

            var person = this.personManager.FindById(id);

            if (person == null)
            {
                return BadRequest();
            }

            personPatch.ApplyTo(person);

            var result = await this.personManager.UpdateAsync(person);

            return Ok(result);
        }
        
    }
}