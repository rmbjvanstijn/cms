﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using System.Web;
using System.Web.Http;

using cms.Models;

namespace cms.Stores
{
    public class PersonStore
    {
        private readonly Database.Database database;

        public PersonStore(Database.Database database)
        {
            this.database = database;
        }

        public List<Person> FindAllASync()
        {
            var allPersons = this.database.Get<Person>("");
            return allPersons;
        }

        public Person FindById(string id)
        {
            var person = this.database.Get<Person>($"WHERE id = {id}");
            return person.FirstOrDefault();
        }

        public async Task<bool> UpdateAsync(Person person)
        {
            var result = await this.database.Update<Person>(person);
            return result;
        }
    }
}